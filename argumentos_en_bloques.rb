class User
	attr_accessor :nombre, :apellido

	def saludar
		yield(@nombre)
	end	

	def despedir
		yield(@nombre, @apellido)
	end	

end	
# retornan la ultima linea del bloque
# no se puede usar el return, es solo para los metodos
luis = User.new
luis.nombre = "Luis"
luis.saludar { |nombre| puts "Hola #{nombre}"}

luis.apellido = "Ramirez"
luis.despedir do |nombre, apellido|
	puts "Adios #{nombre} #{apellido}"
end	
