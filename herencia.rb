class Object
	def tatuco
		puts "TATUCO"
	end	
end

class Video
	attr_accessor :title, :duration, :description

	def embed_video_code
		"<video></video>"
	end	

	def setup(title)
		@title = title
	end	
end


class YouTubeVideo < Video
	attr_accessor :youtube_id

	def embed_video_code
		"<iframe/>"
	end	

	def setup(title)
		super
		# comportamiento extra del hijo
		@title = title + ".mp4"
	end	
end	

class FacebookVideo < Video
	attr_accessor :facebook_id
end	

f = FacebookVideo.new.embed_video_code 
v = YouTubeVideo.new
 v.setup("Video Hijo")

puts v.title
puts v.tatuco

#youtube = YouTubeVideo.new
#youtube.title = "Herencia en Ruby"
#youtube.youtube_id = 1

#puts youtube.title
#puts youtube.youtube_id