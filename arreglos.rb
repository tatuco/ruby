puts "--- Arreglos ----"

arreglo = ["luis", 2, true, String] #inicializar
arreglo_dos = Array.new(5)
arreglo_tres = %w[1 4 'hola'] #dentro del arreglo no es necesario las comas se dividen por espacios

puts arreglo
puts "posicion 0 => #{arreglo[0]}"
puts arreglo_dos
puts "arreglo sin comas $w #{arreglo_tres}"

print "ingrese un valor para almecenar en el arreglo_dos \n"

valor = gets

arreglo_dos << valor

puts "Arreglo #{arreglo_dos}, valor nuevo : #{valor}"
