#concatenar
nombre = "Luis"
cadena = "Hola " + nombre

puts cadena

puts "----------------"
#interpolacion
nombre = "Ramirez"
cadena = "Luis #{nombre.upcase}"

puts cadena

#ver todos los metodos de un objeto string
#puts ''.methods

puts "Luis".downcase
puts "luis".capitalize

puts "----------------"

#\n saltode linea \t tabulacion
puts "Hola Luis \n \n \n " #solo se interpretan con dobles comillas

puts " \t fin"

puts "----------------"
puts "2".to_f
puts "2".to_i
puts 2.to_s