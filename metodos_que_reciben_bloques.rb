def metodo
	yield if block_given?
end	
# yield ejecuta el bloque que le pasemos al metodo como argumento
#  si no embiamos un bloque al metodo el llamado a yield retornara error
# block_given retorna false si elmetodo no recibe un bloque
# yield esmas rapido que guardar el bloque en &bloque y llamarlo
metodo { puts "--Soy un bloque--" }


def metodo_dos &bloque
	#yield if block_given?
	bloque.call if block_given?
end

metodo_dos { puts "Soy el segundo bloque"}	

def otro_metodo &bloque
	metodo_dos(&bloque)
end	


otro_metodo {puts "Soy el segundo bloque"}	
  
