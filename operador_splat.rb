# 
def hola_gente(personas)
	personas.each do |persona|
		puts "Hola #{persona}"
	end	
end	

hola_gente(["Luis", "Maria", 21])

def hola_gente_dos(*personas)# el splat hace que el metodo pueda recibir 
	personas.each do |persona|# una cantidad de elementos indefinida
		puts "Hola #{persona}"
	end	
end	

hola_gente_dos "alberto", "maria", 12, true, 21.2, String

def hola_gente_tres(mensaje, *personas)
	personas.each do |persona| 
		puts "#{mensaje} #{persona}"
	end	
end	

hola_gente_tres "Que hizo?", "jose", "maria", "juan"

# el * convierte un array en una lista de argumentos