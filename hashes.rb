tutor = { 
			"nombre" => "Luis",
			"apellido" => "Ramirez",
			"edad"  => 22
		}

puts tutor["nombre"]

tutor.default = "N/A"
tutor["nueva-clave"] = 123

puts tutor		

profesor = {:nombre => "Luis", :edad =>22, :especialidad => "any"}

puts profesor

tutor.each do |clave, valor|
	puts "clave: #{clave} y valor: #{valor}"
end	