class SoyObjetoLoJuro
	@nombre_clase = "SoyObjetoLoJuro"
#metodo de clase, es como un metodo estatico
#se mandan a llamar sobre la clase y no sobre una instancia
# class << self y se eliminaria el self antes de cada metodo
# se usan cuando la funcionalidad de la clase no se usara en ningun objeto
	def self.nombre_clase
		@nombre_clase
	end

	def self.nombre_clase=(nombre_clase)
		@nombre_clase = nombre_clase
	end	
end


puts SoyObjetoLoJuro.nombre_clase = "Luis"