(lambda { puts "hola mundo" }).call
lambda_uni = ->(nombre) {puts "hola #{nombre}"}

puts lambda_uni.class
puts lambda_uni.call("luis")

# un proc es un bloqie
# una lambda es un metodo

def test_block
	(Proc.new { return "game over"}).call()
	puts "despues del bloque"
end	

def test_lambda
	(->() { return "game over"}).call()
	puts "despues de la lambda"
end	

puts test_block
puts test_lambda