puts "Rangos instancias de la class Range"

numeros = (1..10)
letras = ("a".."z")
puts numeros.class

numeros.each do |numero|
 puts numero
end	

puts "------de dos en dos con step(2)--------otro ciclo --------"
#de dos en dos
numeros.step(2).each do |numero|
	puts numero
end	

puts "------ letras ---------"

letras.each do |letra|
	print "#{letra} ,"
end	

puts "\n---- valores minimos y maximos"
puts numeros.min
puts numeros.max

puts "convertir rango a un arreglo"
puts numeros.to_a.class