# es una cade inmutable
cadena = "Luis"
cadena_dos = "Luis"
simbolo = :Uriel
simbolo_dos = :Uriel

puts cadena.object_id
puts cadena_dos.object_id
puts simbolo.object_id
puts simbolo_dos.object_id
# se usan cuando no necesito modificar el string
# cuando no se usaran los metodos del string
# los simbolos son mas rapidos para compalar ya que tiene el mismo id de objeto


