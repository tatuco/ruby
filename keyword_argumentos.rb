#parametros con nombre para saber en el llamado de un metodo que es lo que va a hacer
#sin tener que ver el cuerpo del metodo

def hola(nombre:"", edad:0)
	if edad > 30
		puts "Hola señor #{nombre}"
	elsif edad < 30
			puts "Hola joven #{nombre}"
	end		
end	

#si no se envia un parametro no retornara error
# porque ya tiene un valor por default en la declaracion del metodo
#no importa el orden en que se envien los parametros
# ya que se identifican por el nombre o clave del parametro
puts hola(nombre:"Luis", edad:22)

# existe el splat para pasar lo que que quiera **parametroslocos

#parametros obligatorios param:
# parametros opcionales param:"default"