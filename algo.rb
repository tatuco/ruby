class PadreController
	attr_accessor :model, :service
	def index
		@service.index
	end	
	def show(id)
		@service.show(id)
	end	
	def upper(name)
		@service.upper(name)
	end	
end	

class HijaController < PadreController
	def initialize
		@service = HijaService.new
	end	
end	

class PadreService

	def index
		puts "listado de #{@model} (respuesta del PadreService)"
	end	

	def show(id)
		puts "#{@model} con Id #{id} (respuesta del PadreService)"
	end
	def upper(name)
		puts name.to_s.upcase
	end	
end	

class HijaService < PadreService
	def initialize
		@model = "usuario"
	end	
end	

hija = HijaController.new
hija.index
hija.show(2)
hija.upper("luis")