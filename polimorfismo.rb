class Video 
	def play
	end	
end	

class Vimeo < Video
	def play
		p "Inserte reproductor Vimeo"
	end	
end	

class YouTube < Video
	def play
		p "Inserte reproductor de YouTube"
	end	
end	
#polimorfismo :  responder a un mensaje de diferente manera deacuerdo a la clase
videos = [Vimeo.new,YouTube.new,YouTube.new,Vimeo.new]

videos.each do |video|
	puts video.play
end