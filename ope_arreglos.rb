arreglo = %w[10 2 34 65 8]
numeros = [2,6,7,1,9,43]
# puts arreglo * 2 duplica el arreglo
#join conveirte un arregloen una cadena
puts arreglo * "-"


puts "------ usando join -------"
puts arreglo.join("+")

puts "------ ordenando  con sort -------"

puts "------strings--------"
puts arreglo.sort()

puts "-------numeros---desc------"
puts numeros.sort()
puts "-------numeros---asc------"
puts numeros.sort().reverse

puts " buscar un elemnto "
puts numeros.include?(10)

puts " primer elemnto"
puts arreglo.first

puts "ultimo elemnto"
puts arreglo.last

puts "el mismo arreglo sin elemntos repetidos"

puts arreglo.uniq

puts " de vuelve un elemento del arreglo aleatoriamente"
puts arreglo.sample