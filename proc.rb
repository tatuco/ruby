# los proc son comolos bloques, son instancias de la clase Proc
# cuando le ponemos a un metodo &bloque este crea una instancia de la clase Proc
# usarlos cuando se requiere recibir mas de un bloque en un metodo

def hola &bloque
	puts bloque.class.name
	bloque.call
end

hola { puts "kjasnbfksdf" }	



def hola_dos proc_uno, proc_dos
	proc_uno.call
	proc_dos.call
end

proc_uno = Proc.new { puts "soy un proc" }
proc_dos = Proc.new { puts "soy el segundo proc" }

hola_dos(proc_uno,proc_dos)	