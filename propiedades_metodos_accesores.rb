class Tutor
	attr_accessor: name#get y set
	attr_reader: name#get
	attr_writter: name#set
	def initialize(name)
		@name = name #variable de instancia es como el this.
	end	

	def name#get
		puts @name
	end	

	def name=(name)#set
		@name = name
	end	
end	
#con los attr no son necesarios los metodos geter y seter explicitos en la clase
# en ruby los metodos accesores no usan get o set
#
luis = Tutor.new("luis")
alberto = Tutor.new("alberto")

puts luis.name
puts luis.name="Luis Jose"