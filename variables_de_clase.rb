class Video

	@@var_clase = "Soy variable de Clase"
	@var_objeto = "Soy variable de Objeto"

end	
#guardan al misma referencia tanto en el padre como en el hijo
# si semodifica en el hijo tambien cambian en el padre
class YouTube < Video

	def self.test
		p @@var_clase = "Variable de clase Modificada por Hijo"
		p @@var_clase 
		p @var_objeto #solo le pertenece a la clase video
	end	

end	

puts YouTube.test