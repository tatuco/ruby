class Humano

	def initialize
		#self.publico
		privado
	end	

	def publico
		puts "soy un metodo publico"
	end	

	private # metodos llamados solo dentro de la misma clase y de las hijas
		def privado
			puts "soy privado"
		end	
	protected # llamadas dentro de la clase y de los hijos
		def protegido
			puts "soy protegido"
		end	
# is_a? es instancia de una clase determinada?
end

class Tutor < Humano
	def initialize
		#privado
		protegido
	end	
end	

Humano.new
Tutor.new.publico